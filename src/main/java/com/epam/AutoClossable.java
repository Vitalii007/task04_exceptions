package com.epam;

import com.epam.controller.Controller;
import com.epam.controller.ControllerClass;
import com.epam.view.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class AutoClossable {

    private String gameResult;
    Controller control = new ControllerClass();

    public static void main(String[] args) {

        new MyView().show();
        new AutoClossable().close();
    }

    public void close() {

        File file = new File("result.txt");
        try (FileOutputStream fileOutputStream = new FileOutputStream(file);) {
            gameResult = control.showUpgradingDetails();
            fileOutputStream.write(gameResult.getBytes());
            if (file.length() == 0) {
                throw new FileException();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (FileException e) {
            e.printStackTrace();
        }
    }
}
