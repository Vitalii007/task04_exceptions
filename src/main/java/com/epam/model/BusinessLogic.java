package com.epam.model;

public class BusinessLogic implements Model {

    private Domain domain;

    public BusinessLogic() {
        domain = new Domain();
    }


    @Override
    public String upgradeBase() {
        return domain.upgradeBase();
    }

    @Override
    public String upgradeBank() {
        return domain.upgradeBank();
    }

    @Override
    public String upgradeShrine() {
        return domain.upgradeShrine();
    }

    @Override
    public String showBuildingsState() {
        return "Your buildings:\n" + domain.showBuildingsState();
    }

    @Override
    public String showResourcesState() {
        return "Your resources:\n" + domain.showResourcesState();
    }

    @Override
    public String showUpgradingDetails() {
        return domain.showUpgradingDetailsBase() + domain.showUpgradingDetailsBank()
                + domain.showUpgradingDetailsShrine();
    }

    @Override
    public String winCheck(){
        return domain.winCheck();
    }
}