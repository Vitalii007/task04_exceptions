package com.epam.model;

import com.epam.NotEnoughMaterials;

import java.util.Random;

public class Domain {

    private int levelBase = 1;
    private int levelBank = 1;
    private int levelShrine = 1;
    private int amountGold;
    private int amountChakra;

    public Domain() {
        Random random = new Random();
        amountGold = (int) (9000 * random.nextDouble() + 1000);
        amountChakra = (int) (9000 * random.nextDouble() + 1000);
    }

    public String upgradeBase() {
        try {
            switch (levelBase) {
                case 1:
                    if (amountGold < 500 || amountChakra < 500) {
                        throw new NotEnoughMaterials();
                    } else {
                        amountGold -= 500;
                        amountChakra -= 500;
                        levelBase += 1;
                        return "Upgrade Base to level 2 is successful!";
                    }
                case 2:
                    if (amountGold < 1500 || amountChakra < 1500) {
                        throw new NotEnoughMaterials();
                    } else {
                        amountGold -= 1500;
                        amountChakra -= 1500;
                        levelBase += 1;
                        return "Upgrade Base to level 3 is successful!";
                    }
                case 3:
                    if (amountGold < 4000 || amountChakra < 4000) {
                        throw new NotEnoughMaterials();
                    } else {
                        amountGold -= 4000;
                        amountChakra -= 4000;
                        levelBase += 1;
                        return "Upgrade Base to level 4 is successful!";
                    }
                default:
                    return "Your Base level is max";
            }
        } catch (NotEnoughMaterials e1) {
            return "Not enough materials to upgrade Base";
        }
    }

    public String upgradeBank() {
        try {
            switch (levelBank) {
                case 1:
                    if (amountChakra < 1000) {
                        throw new NotEnoughMaterials();
                    } else {
                        amountGold += 1500;
                        amountChakra -= 1000;
                        levelBank += 1;
                        return "Upgrade Bank to level 2 is successful!";
                    }
                case 2:
                    if (amountChakra < 1500) {
                        throw new NotEnoughMaterials();
                    } else {
                        amountGold += 2500;
                        amountChakra -= 1500;
                        levelBank += 1;
                        return "Upgrade Bank to level 3 is successful!";
                    }
                case 3:
                    if (amountChakra < 2500) {
                        throw new NotEnoughMaterials();
                    } else {
                        amountGold += 4000;
                        amountChakra -= 2500;
                        levelBank += 1;
                        return "Upgrade Bank to level 4 is successful!";
                    }
                default:
                    return "Your Bank level is max";
            }
        } catch (NotEnoughMaterials e2) {
            return "Not enough materials to upgrade Bank";
        }
    }

    public String upgradeShrine() {
        try {
            switch (levelShrine) {
                case 1:
                    if (amountGold < 1000) {
                        throw new NotEnoughMaterials();
                    } else {
                        amountChakra += 1500;
                        amountGold -= 1000;
                        levelShrine += 1;
                        return "Upgrade Shrine to level 2 is successful!";
                    }
                case 2:
                    if (amountGold < 1500) {
                        throw new NotEnoughMaterials();
                    } else {
                        amountChakra += 2500;
                        amountGold -= 1500;
                        levelShrine += 1;
                        return "Upgrade Shrine to level 3 is successful!";
                    }
                case 3:
                    if (amountGold < 2500) {
                        throw new NotEnoughMaterials();
                    } else {
                        amountChakra += 4000;
                        amountGold -= 2500;
                        levelShrine += 1;
                        return "Upgrade Shrine to level 4 is successful!";
                    }
                default:
                    return "Your Shrine level is max";
            }
        } catch (NotEnoughMaterials e3) {
            return "Not enough materials to upgrade Shrine";
        }
    }

    public String showBuildingsState() {
        return "Level of Base - " + levelBase + ", Level of Bank - "
                + levelBank + ", Level of Shrine - " + levelShrine;
    }

    public String showResourcesState() {
        return "Gold - " + amountGold + ", Chakra - " + amountChakra;
    }

    public String showUpgradingDetailsBase() {
        switch (levelBase) {
            case 1:
                return "Upgrade Base to level 2 cost 500 gold and "
                        + "500 chakra\n";
            case 2:
                return "Upgrade Base to level 3 cost 1500 gold and "
                        + "1500 chakra\n";
            case 3:
                return "Upgrade Base to level 4 cost 4000 gold and "
                        + "4000 chakra\n";
            default:
                return "Your Base level is max\n";
        }
    }

    public String showUpgradingDetailsShrine() {
        switch (levelShrine) {
            case 1:
                return "Upgrade Shrine to level 2 cost 1000 gold and "
                        + "give you bonus +1500 chakra\n";
            case 2:
                return "Upgrade Shrine to level 3 cost 1500 gold and "
                        + "give you bonus +2500 chakra\n";
            case 3:
                return "Upgrade Shrine to level 4 cost 2500 gold and "
                        + "give you bonus +4000 chakra\n";
            default:
                return "Your Shrine level is max\n";
        }
    }

    public String showUpgradingDetailsBank() {
        switch (levelBank) {
            case 1:
                return "Upgrade Bank to level 2 cost 1000 chakra and "
                        + "give you bonus +1500 gold\n";
            case 2:
                return "Upgrade Bank to level 3 cost 1500 chakra and "
                        + "give you bonus +2500 gold\n";
            case 3:
                return "Upgrade Bank to level 4 cost 2500 chakra and "
                        + "give you bonus +4000 gold\n";
            default:
                return "Your Bank level is max\n";
        }
    }

    public String winCheck() {
        if (levelBase == 4) {
            return "Congratulations! You a winner!";
        } else return null;
    }
}
