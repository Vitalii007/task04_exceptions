package com.epam.model;

public interface Model {

    String upgradeBase();
    String upgradeBank();
    String upgradeShrine();
    String showBuildingsState();
    String showResourcesState();
    String showUpgradingDetails();
    String winCheck();
}
