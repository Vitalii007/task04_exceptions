package com.epam.controller;

import com.epam.model.*;

public class ControllerClass implements Controller {

    private Model model;

    public ControllerClass() {

        model = new BusinessLogic();
    }

    @Override
    public String upgradeBase() {
        return model.upgradeBase();
    }

    @Override
    public String upgradeBank() {
        return model.upgradeBank();
    }

    @Override
    public String upgradeShrine() {
        return model.upgradeShrine();
    }

    @Override
    public String showBuildingsState() {
        return model.showBuildingsState();
    }

    @Override
    public String showResourcesState() {
        return model.showResourcesState();
    }

    @Override
    public String showUpgradingDetails() {
        return model.showUpgradingDetails();
    }

    @Override
    public String winCheck(){
        return model.winCheck();
    }
}
