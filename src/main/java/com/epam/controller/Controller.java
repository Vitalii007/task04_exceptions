package com.epam.controller;

public interface Controller {
    String upgradeBase();
    String upgradeBank();
    String upgradeShrine();
    String showBuildingsState();
    String showResourcesState();
    String showUpgradingDetails();
    String winCheck();
}
