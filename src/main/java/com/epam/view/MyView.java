package com.epam.view;

import com.epam.controller.*;

import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.SocketHandler;

public class MyView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner scanner = new Scanner(System.in);

    public MyView() {
        controller = new ControllerClass();
        System.out.println(controller.showBuildingsState());
        System.out.println(controller.showResourcesState());
        System.out.println("Upgrade Base level to 4 to win game!");
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - upgrade Base");
        menu.put("2", "  2 - upgrade Bank");
        menu.put("3", "  3 - upgrade Shrine");
        menu.put("4", "  4 - print buildings levels");
        menu.put("5", "  5 - print amount of gold and chakra");
        menu.put("6", "  6 - print upgrading rules");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
    }

    private void pressButton1() {
        System.out.println(controller.upgradeBase());
    }

    private void pressButton2() {
        System.out.println(controller.upgradeBank());
    }

    private void pressButton3() {
        System.out.println(controller.upgradeShrine());
    }

    private void pressButton4() {
        System.out.println(controller.showBuildingsState());
    }

    private void pressButton5() {
        System.out.println(controller.showResourcesState());
    }

    private void pressButton6() {
        System.out.println(controller.showUpgradingDetails());
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while ((!keyMenu.equals("Q")) && (controller.winCheck() == null));
        if (controller.winCheck() !=  null){
            System.out.println(controller.winCheck());
        }
    }
}